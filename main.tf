terraform {
  required_version = "= 0.12.20"
}

provider "gitlab" {
    token = var.gitlab_token
    version = "~> 2.5.0"
}

provider "aws" {
  region = "us-east-2"
  version = "~> 2.0"
}

terraform {
  backend "s3" {
    bucket = "mf-terraform-gitlab-exercise-state"
    key = "global/s3/terraform.tfstate"
    region = "us-east-2"
    dynamodb_table = "terraform-exercise-locks"
    encrypt = true
  }
}

resource "aws_s3_bucket" "terraform_state" {
  bucket = "mf-terraform-gitlab-exercise-state"
  lifecycle {
    prevent_destroy = true
  }
  versioning {
    enabled = true
  }
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }
}
resource "aws_dynamodb_table" "terraform_locks" {
  name = "terraform-gitlab-exercise-locks"
  billing_mode = "PAY_PER_REQUEST"
  hash_key = "LockID"
  attribute {
    name = "LockID"
    type = "S"
  }
}

# Add a project owned by the user
resource "gitlab_project" "sample_project" {
    name = "example-with-gitlab-terraform-provider"
    visibility_level = "public"
}

data "gitlab_user" "example" {
  username = var.gitlab_username
}

resource "aws_instance" "example" {
  ami = "ami-0c55b159cbfafe1f0"
  instance_type = "t2.micro"
  vpc_security_group_ids = [aws_security_group.instance.id]
  user_data = data.template_file.user_data.rendered
  tags = {
    Name = "mf-terraform-gitlab"
  }
}

data "template_file" "user_data" {
  template = file("${path.module}/user-data.sh")
  vars = {
    server_port = var.server_port
    name = data.gitlab_user.example.name
    avatar = data.gitlab_user.example.avatar_url
  }
}

resource "aws_security_group" "instance" {
  name = "mf-gitlab-terraform-instance"
  ingress {
    from_port = var.server_port
    to_port = var.server_port
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}