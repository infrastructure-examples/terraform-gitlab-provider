output "public_ip_with_port" {
  description = "IP with port to check for the output"
  value = "${aws_instance.example.public_ip}:${var.server_port}"
}
