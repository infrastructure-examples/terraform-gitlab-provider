Example learning Terraform project using:
- [Terraform GitLab provider](https://www.terraform.io/docs/providers/gitlab/index.html)
- [Terrafom AWS provider](https://www.terraform.io/docs/providers/aws/index.html)

The state is stored on AWS S3.

The project is set up with GitLab CI/CD pipelines.
For authentication with providers, the AWS Access Key and AWS Secret Key as well as GitLab Personal Access Token have to be set in the `Settings >> CI/CD >> Variables` (`VAR_TF_gitlab_token` as key for the GitLab PAT).

Another required input variable is an username of a GitLab user. You can set the `VAR_TF_gitlab_username` in the `Settings >> CI/CD >> Variables` as well, or add it on the pipelines run; value should be some GitLab username, like `m_frankiewicz`.

The code is creating a project on GitLab, with the project name `example-with-gitlab-terraform-provider`, if such a project doesn't exist yet in a given namespace.
It also creates a busybox server on AWS EC2 instance that serves index page with name and avatar image of the GitLab user, whose username was previously given.

Output variable shows where to see that index page.

This project doesn't have much sense of course, so don't look for the meaning ;)

