#!/bin/bash
cat > index.html <<EOF
<h1>Hello World!</h1>
<p>Greetings form the user ${name}!</p>
<img src="${avatar}" alt="Avatar of the user ${name}" height="250">
EOF
nohup busybox httpd -f -p ${server_port} &