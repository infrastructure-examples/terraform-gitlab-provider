variable "gitlab_token" {
  description = "Personal access token for GitLab"
  type = string
}

variable "gitlab_username" {
  description = "Username of a GitLab user that you wnat to see info about"
  type = string
}

variable "server_port" {
  description = "The port the server will use for HTTP requests"
  type = number
  default = 8080
}
